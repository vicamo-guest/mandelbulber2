Source: mandelbulber2
Section: math
Priority: optional
Maintainer: Giovanni Mascellani <gio@debian.org>
Build-Depends: debhelper (>= 11)
Build-Depends-Arch: qtbase5-dev, qtmultimedia5-dev, libpng-dev,
 libgsl-dev, qttools5-dev, qttools5-dev-tools, opencl-dev,
 opencl-clhpp-headers, liblzo2-dev, qt5-qmake:native
Build-Depends-Indep: docbook-xsl, xsltproc, texlive-plain-generic,
 texlive-latex-extra, texlive-pictures, texlive-fonts-recommended,
 lmodern, ghostscript
Standards-Version: 4.1.4
Homepage: https://github.com/buddhi1980/mandelbulber2
Vcs-Browser: https://salsa.debian.org/debian/mandelbulber2
Vcs-Git: https://salsa.debian.org/debian/mandelbulber2.git

Package: mandelbulber2
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, mandelbulber2-data (= ${source:Version})
Replaces: mandelbulber2-data (<< 2.13.2-4)
Description: 3D fractal renderer and animator
 Mandelbulber2 is a ray-tracing application for drawing three-dimensional
 fractals, like Mandelbulb, Mandelbox, Julia, trigonometric, hypercomplex
 or IFS fractals. It is highly customizable and features complex shading
 algorithms (among the others there are shadows, depth of field, ambient
 occlusion).
 .
 Mandelbulber2 is also able to produce animations and has a simple
 built-in 3D navigator for exploring the fractals.

Package: mandelbulber2-data
Architecture: all
Depends: ${misc:Depends}
Replaces: mandelbulber2 (<< 2.13.2-4)
Description: 3D fractal renderer and animator - data files
 Mandelbulber2 is a ray-tracing application for drawing three-dimensional
 fractals, like Mandelbulb, Mandelbox, Julia, trigonometric, hypercomplex
 or IFS fractals. It is highly customizable and features complex shading
 algorithms (among the others there are shadows, depth of field, ambient
 occlusion).
 .
 Mandelbulber2 is also able to produce animations and has a simple
 built-in 3D navigator for exploring the fractals.
 .
 This package contains architecture-independent data files for
 Mandelbulber2.
