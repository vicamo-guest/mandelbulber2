

\section{OpenCL}\label{opencl}\index{OpenCL}

The use of OpenCL enables offloading the rendering of the fractal to the
GPU or to an accelerator card. This can highly reduce the render time.
OpenCL itself is an industry standard developed by the Khronos group and
is a well established framework. The two mayor GPU vendors (ATI / Nvidia)
among others implement the OpenCL specification in their drivers.
OpenCL uses single precision floating point accuracy, so the minimum camera to surface distance is about 1e-05, whereas  this distance can be reduced to about 1e-09 with openCL disabled. Therefore openCL is not suitable for zooming down into fine detailed areas.

\subsection{Setup of OpenCL}\label{setup-opencl}
To render in Mandelbulber with OpenCL you will need to install a recent driver.
The newest GPU driver available can be obtained from the links in the next table.
Choose your operation system and misc settings.
Then download and install the driver image.

\begin{center}
	\begin{tabular}{ | l | r | }
		\hline
		AMD 	&
		\href{http://support.amd.com/en-us/download/}{http://support.amd.com/en-us/download/}
		\\ \hline
		Nvidia 	& 
		\href{http://www.nvidia.de/Download/index.aspx}{http://www.nvidia.de/Download/index.aspx}
		\\ \hline
		Intel	&
		\href{https://downloadcenter.intel.com}{https://downloadcenter.intel.com}
		\\ \hline
	\end{tabular}
\end{center}

You should also be able to use free drivers, if they support OpenCL.
Sadly the performance of those drivers is typically below the performance of the proprietary ones.

\subsubsection{Setup of OpenCL on Windows}\label{setup-opencl-windows}
With a recent driver, a capable GPU and the Mandelbulber OpenCL version you are already set.
Proceed with \ref{configure-opencl}. Note: Windows users may need to edit the registry to avoid timeout errors, refer \ref{opencl-troubleshooting}

\subsubsection{Setup of OpenCL on Linux}\label{setup-opencl-linux}
With a recent driver, a capable GPU and the Mandelbulber OpenCL version you are already set.
Proceed with \ref{configure-opencl}. 
If you are a developer and compile your own Mandelbulber version
you need to have the package \textbf{opencl-headers} installed in the system.  
See also the corresponding README.

\subsubsection{Setup of OpenCL on MacOS}\label{setup-opencl-macos}
TODO

\subsection{Configuring OpenCL}\label{configure-opencl}
Open Mandelbulber and navigate to: Menu > File > Program Preferences > OpenCL (GPU).
You will find the configuration page in figure \ref{opencl_tab}.

\simpleImageWithCaption75Width{img/manual/media/opencl_tab.png}
{OpenCL Tab in preferences}
{opencl_tab}

\begin{itemize}
	\item First you need to enable OpenCL by enabling the checkbox
	\item Then you need to select the platform and device to identify the OpenCL hardware 
		element to render with.
%	\item \textbf{Precision} - Switch between float and double precision
%	\begin{itemize}
%		\item \textbf{float} - gives high performance render, but is limited in precision, 
%			sometimes this mode can result in artifacts and bad DE.
%		\item \textbf{double}- gives lower performance render (at least by factor 2), but has the same accuracy as the non OpenCL version. This mode may prevent artifacts.
%	\end{itemize}
%	\item \textbf{Mode} - Switches the rendering engine between different levels of shader extent.	
	\item \textbf{Memory Limit} - Memory limit in MB for the device. Most graphics cards cannot handle memory objects larger than 512MB. If you observe that SSAO or DOF effects fail during rendering you can try to decrease this limit. When the program needs more memory (image resolution too high), then the effects will be rendered using CPU.
	\item \textbf{Mode} - This setting is located in the Navigation dock just below the RENDER button. It switches the rendering engine between different levels of shader extent.
	\begin{itemize}
		\item  \textbf{no OpenCL} - temporarily disables OpenCL 
		\item  \textbf{fast} - renders preview of the fractal shape. The appearance of the fractal is not realistic, but the rendering is very fast.
		\item  \textbf{limited} - image is rendered with most of the effects, except reflection and refraction of light. This mode offers realistic rendering, but uses less graphics card resources than \emph{full} mode. 
		\item  \textbf{full} - renders all effects		
	\end{itemize}

Note: When openCL is enabled the image will be rendered in \textbf{fast mode}, a user must change to limited or full modes to get a realistic appearance. The default setting is \textbf{fast mode}  because on low cost gfx cards (like built-in Intel), \textbf{full} mode doesn't work and can cause the gfx driver to crash. 	

	
\end{itemize}
	\simpleImageWithCaptionHalfWidth{img/manual/media/opencl_mode.png}{OpenCL Mode in Navigation dock}
	{opencl_mode}

\subsection{Trouble shooting OpenCL}\label{opencl-troubleshooting}

\subsubsection{Driver crash under Windows}

When Mandelbulber is run under Windows, the OS will monitor the GPU with a watchdog. When the card becomes unresponsive for more than two seconds, the driver will shutdown and crash with a message like:

\textbf{The NVIDIA OpenGL driver lost connection with the display driver due to exceeding the Windows Time-Out limit and is unable to continue.}

It can happen when there are enabled effects which make rendering of each pixel very long. 

A workaround for this problem is to increase this timeout limit. To do so you need to add or modify two keys in the windows registry. Beware: Do this at your own risk, changing any wrong keys in the windows registry may cause windows to stop working properly!

Change of registry takes effect after restart of Windows.

\begin{enumerate}
	
	\item \textbf{Open registry editor:} [Start] > Run > Type in "Regedit" > Hit Enter
	\item \textbf{Navigate to key:} Open \emph{HKEY\_LOCAL\_MACHINE > System > CurrentControlSet > Control > GraphicsDrivers}
	\item \textbf{Create the keys (Modify if exist):}
	\begin{enumerate}
		\item Create key of type \textbf{DWORD (32-bit)} and name \textbf{TdrDelay} with a value of \textbf{30} as Decimal value.
		\item Create key of type \textbf{DWORD (32-bit)} and name \textbf{TdrDdiDelay} with a value of \textbf{30} as Decimal value.	
	\end{enumerate}	
	\item \textbf{Reboot}

\end{enumerate}

You can find more information about this topic in the following resources:

The original Blogpost in Fragmentarium, which has been used as the source of this article: \url{http://blog.hvidtfeldts.net/index.php/2011/12/fragmentarium-faq/}

Microsoft Explanation about the affected registry keys: \url{https://docs.microsoft.com/en-us/windows-hardware/drivers/display/tdr-registry-keys}

Conversation about this topic in the fractalforums Mandelbulber group:
\url{http://www.fractalforums.com/feature-requests/render-bucket-size-control-for-opencl/msg102868/#new}

Battlefield trouble shooting with same problem:
\url{https://www.reddit.com/r/battlefield_4/comments/1xzzn4/tdrdelay_10_fixed_my_crashes_since_last_patch/}

\subsubsection{Artifacts from glow and fog}

 Limited floating point accuracy can cause artifacts can when using glow or fog, if the Maximum View Distance is too large. In the image below the distance was set at 192, reducing this to 30 removes the visible artifacts.
 
 	\simpleImageWithCaptionHalfWidth{img/manual/media/artifacts_from_glow_or_fog.png}{artifacts from glow or fog.png}
 		{artifacts_from_glow_or_fog}
 
 